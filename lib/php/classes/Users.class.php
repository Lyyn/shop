<?php
    class Users {
        private $id_users;
        private $id_pays;
        private $login;
        private $nom_affichage;
        private $pass;
        private $email;
        private $date_insc;
        private $adresse;
        private $code_postal;
        private $state;

        /**
         * Users constructor.
         * @param $id_users
         * @param $id_pays
         * @param $login
         * @param $nom_affichage
         * @param $pass
         * @param $email
         * @param $date_insc
         * @param $adresse
         * @param $code_postal
         * @param $state
         */
        public function __construct($id_users = null, $id_pays = null, $login = null, $nom_affichage = null, $pass = null, $email = null, $date_insc = null, $adresse = null, $code_postal = null, $state = null) {
            $this->id_users = $id_users;
            $this->id_pays = $id_pays;
            $this->login = $login;
            $this->nom_affichage = $nom_affichage;
            $this->pass = $pass;
            $this->email = $email;
            $this->date_insc = $date_insc;
            $this->adresse = $adresse;
            $this->code_postal = $code_postal;
            $this->state = $state;
        }

        /**
         * Renvoie une array contenant tous les attributs de l'objet
         * @return array()
         */
        public function getArray() {
            return array(
                'id_users'      => $this->id_users,
                'id_pays'       => $this->id_pays,
                'login'         => $this->login,
                'nom_affichage' => $this->nom_affichage,
                'pass'          => $this->pass,
                'email'         => $this->email,
                'date_insc'     => $this->date_insc,
                'adresse'       => $this->adresse,
                'code_postal'   => $this->code_postal,
                'state'         => $this->state
            );
        }

        public function setArray($array) {
            if(!empty($array['id_users']))      $this->id_users = $array['id_users'];
            if(!empty($array['id_pays']))       $this->id_pays = $array['id_pays'];
            if(!empty($array['login']))         $this->login = $array['login'];
            if(!empty($array['nom_affichage'])) $this->nom_affichage = $array['nom_affichage'];
            if(!empty($array['pass']))          $this->pass = $array['pass'];
            if(!empty($array['email']))         $this->email = $array['email'];
            if(!empty($array['date_insc']))     $this->date_insc = $array['date_insc'];
            if(!empty($array['adresse']))       $this->adresse = $array['adresse'];
            if(!empty($array['code_postal']))   $this->code_postal = $array['code_postal'];
            if(!empty($array['state']))         $this->state = $array['state'];
        }


        /**
         * @return null
         */
        public function getIdUsers()
        {
            return $this->id_users;
        }

        /**
         * @param null $id_users
         */
        public function setIdUsers($id_users)
        {
            $this->id_users = $id_users;
        }

        /**
         * @return null
         */
        public function getIdPays()
        {
            return $this->id_pays;
        }

        /**
         * @param null $id_pays
         */
        public function setIdPays($id_pays)
        {
            $this->id_pays = $id_pays;
        }

        /**
         * @return null
         */
        public function getLogin()
        {
            return $this->login;
        }

        /**
         * @param null $login
         */
        public function setLogin($login)
        {
            $this->login = $login;
        }

        /**
         * @return null
         */
        public function getNomAffichage()
        {
            return $this->nom_affichage;
        }

        /**
         * @param null $nom_affichage
         */
        public function setNomAffichage($nom_affichage)
        {
            $this->nom_affichage = $nom_affichage;
        }

        /**
         * @return null
         */
        public function getPass()
        {
            return $this->pass;
        }

        /**
         * @param null $pass
         */
        public function setPass($pass)
        {
            $this->pass = $pass;
        }

        /**
         * @return null
         */
        public function getEmail()
        {
            return $this->email;
        }

        /**
         * @param null $email
         */
        public function setEmail($email)
        {
            $this->email = $email;
        }

        /**
         * @return null
         */
        public function getDateInsc()
        {
            return $this->date_insc;
        }

        /**
         * @param null $date_insc
         */
        public function setDateInsc($date_insc)
        {
            $this->date_insc = $date_insc;
        }

        /**
         * @return null
         */
        public function getAdresse()
        {
            return $this->adresse;
        }

        /**
         * @param null $adresse
         */
        public function setAdresse($adresse)
        {
            $this->adresse = $adresse;
        }

        /**
         * @return null
         */
        public function getCodePostal()
        {
            return $this->code_postal;
        }

        /**
         * @param null $code_postal
         */
        public function setCodePostal($code_postal)
        {
            $this->code_postal = $code_postal;
        }

        /**
         * @return null
         */
        public function getState()
        {
            return $this->state;
        }

        /**
         * @param null $state
         */
        public function setState($state)
        {
            $this->state = $state;
        }
    }
?>