<?php
    class Panier {
        private $cupcakes = array();         // Array(id_paniers, Cupcake, Qt)

        /**
         * Panier constructor.
         * @param array $cupcakeList
         */
        public function __construct(array $cupcakeList = null)
        {
            if($cupcakeList != null) {
                $this->cupcakes = $cupcakeList;
            }
            else {
                $this->cupcakes = array();
            }
        }

        /**
         * @return array
         */
        public function getCupcakes()
        {
            return $this->cupcakes;
        }

        /**
         * @param array $cupcakes
         */
        public function setCupcakes($cupcakes)
        {
            $this->cupcakes = $cupcakes;
        }

        /**
         * @param array(array) $item
         * array(id_cupcake, quantite)
         */
        public function addToList($item) {
            array_push($this->cupcakes, $item);
        }

        /**
         * @return int Nombre d'éléments dans le panier
         */
        public function getPanierSize() {
            return sizeof($this->cupcakes);
        }
    }
?>