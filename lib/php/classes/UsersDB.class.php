<?php
    include_once('Users.class.php');

    class UsersDB {
        private $connexion;
        private $user;

        /**
         * UsersDB constructor.
         * @param PDO $connexion
         * @param Users $user
         */
        public function __construct($connexion, $user = null) {
            $this->connexion = $connexion;
            $this->user = $user;
        }

        public function read($id_users) {
            $query = 'SELECT * FROM users WHERE id_users = :id';
            $preparedQuery = $this->connexion->prepare($query);
            $preparedQuery->bindParam(':id', $id_users, PDO::PARAM_INT);

            if(!$preparedQuery->execute()) {
                return false;
            }
            else {
                $result = $preparedQuery->fetch(PDO::FETCH_ASSOC);
                $this->user = new Users(
                    $result['id_users'],
                    $result['id_pays'],
                    $result['login'],
                    $result['nom_affichage'],
                    $result['pass'],
                    $result['email'],
                    $result['date_insc'],
                    $result['adresse'],
                    $result['code_postal'],
                    $result['state']
                );

                return true;
            }
        }

        public function peutCommander() {
            $u = $this->user;

            // On ne peut pas utiliser de empty() sur des méthodes, on DOIT les mettres dans des variables au préalable. *facepalm*
            $a = $u->getAdresse();
            $b = $u->getCodePostal();
            $c = $u->getIdPays();
            return(!empty($a) && !empty($b) && !empty($c));
        }

        /**
         * @return mixed
         */
        public function getConnexion()
        {
            return $this->connexion;
        }

        /**
         * @param mixed $connexion
         */
        public function setConnexion($connexion)
        {
            $this->connexion = $connexion;
        }

        /**
         * @return null
         */
        public function getUser()
        {
            return $this->user;
        }

        /**
         * @param null $user
         */
        public function setUser($user)
        {
            $this->user = $user;
        }
    }
?>