<?php

include_once('Panier.class.php');
include_once('CupcakeDB.class.php');


class PanierDB {
    private $connexion;     // Connexion à la DB
    private $panier;        // Array('id_paniers', 'cupcake', 'quantite')

    /**
     * PanierDB constructor.
     * @param PDO $connexion
     * @param Array() $panier
     */
    public function __construct($connexion, $panier = null) {
        $this->connexion = $connexion;
        if($panier != null) {
            $this->panier = $panier;
        }
        else {
            $this->panier = array();
        }
    }

    /**
     * @return Panier
     */
    public function getPanier() {
        return $this->panier;
    }

    /**
     * @param Panier $panier
     */
    public function setPanier($panier) {
        $this->panier = $panier;
    }

    public function create($id_user, $idProduit, $quantite) {

        $query = 'INSERT INTO paniers (id_users, id_produits, quantite) VALUES (:userid, :produit, :qt)';
        $preparedQuery = $this->connexion->prepare($query);

        $preparedQuery->bindParam(':userid', $id_user, PDO::PARAM_INT);
        $preparedQuery->bindParam(':produit', $idProduit, PDO::PARAM_INT);
        $preparedQuery->bindParam(':qt', $quantite, PDO::PARAM_INT);

        $result = $preparedQuery->execute();
        //print_r($this->connexion->errorInfo());


        if($result) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Récupérer tous les items dans le panier d'un user (via son id)
     * @param int $id_user
     * @param PDO $connexion
     * @return bool
     */
    public function read($id_user, $connexion = null) {
        $query = 'SELECT * FROM paniers WHERE id_users = :id_user';
        $preparedQuery = $this->connexion->prepare($query);
        $preparedQuery->bindParam(':id_user', $id_user, PDO::PARAM_INT);

        if(!$preparedQuery->execute()) {
            return false;
        }
        else {
            $temp = array();
            if(empty($this->panier)) $this->panier = new Panier();

            $result = $preparedQuery->fetchAll(PDO::FETCH_ASSOC);
            foreach($result as $item) {
                $tempCake = new CupcakeDB($this->connexion);
                $tempCake->read($item['id_produits']);
                array_push($temp, array(
                    'id_paniers'    => $item['id_paniers'],
                    'cupcake'       => $tempCake,
                    'quantite'      => $item['quantite']
                ));
            }
            $this->panier->setCupcakes($temp);
            return true;
        }
    }

    public function delete($id_paniers) {
        $query = 'DELETE FROM paniers WHERE id_paniers = :id';
        $preparedQuery = $this->connexion->prepare($query);
        $preparedQuery->bindParam(':id', $id_paniers, PDO::PARAM_INT);
        return $preparedQuery->execute();
    }

    /**
     * Supprime tout le panier d'un utilisateur
     * @param int $id_user
     * @return bool
     */
    public function delFromUser($id_user) {
        $query = 'DELETE FROM paniers WHERE id_users = :id';
        $preparedQuery = $this->connexion->prepare($query);
        $preparedQuery->bindParam(':id', $id_user, PDO::PARAM_INT);
        return $preparedQuery->execute();
    }
}