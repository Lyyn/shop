<?php
    include_once('Commandes.class.php');

    class CommandesDB {
        private $connexion;
        private $commande;

        /**
         * CommandesDB constructor.
         * @param PDO $connexion
         * @param Commandes $commande
         */
        public function __construct($connexion = null, $commande = null)
        {
            $this->connexion = $connexion;
            $this->commande = $commande;
        }

        /**
         * @param Commandes $commande
         * @return bool
         */
        public function create($commande) {
            if(!empty($commande) && !empty($this->connexion)) {
                if(!$commande->isRecordable()) {
                    return false;
                }
                else {
                    $query = '
                        INSERT INTO commandes(id_users, id_produits, qt_produits, prix_total, date_commande)
                        VALUES(:idUser, :idProduits, :qtProduits, \''.$commande->getPrixTotal().'\', \''.date('d/m/Y').'\')
                    ';

                    $preparedQuery = $this->connexion->prepare($query);

                    $idUser     = $commande->getIdUsers();
                    $idProduit  = $commande->getIdProduits();
                    $qtProduit  = $commande->getQtProduits();

                    $preparedQuery->bindParam(':idUser', $idUser, PDO::PARAM_STR);
                    $preparedQuery->bindParam(':idProduits', $idProduit, PDO::PARAM_STR);
                    $preparedQuery->bindParam(':qtProduits', $qtProduit, PDO::PARAM_STR);

                    if($preparedQuery->execute()) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
            else {
                return false;
            }
        }

        public function read($idFacture){
            if($this->connexion == null) {
                return false;
            }
            else {
                $query = 'SELECT * FROM commandes WHERE id_commandes = :idFacture';
                $preparedQuery = $this->connexion->prepare($query);
                $preparedQuery->bindParam(':idFacture', $idFacture, PDO::PARAM_INT);

                if($preparedQuery->execute()) {
                    $data = $preparedQuery->fetch(PDO::FETCH_ASSOC);
                    $this->commande = new Commandes(
                        $data['id_commandes'],
                        $data['id_users'],
                        $data['id_produits'],
                        $data['qt_produits'],
                        $data['prix_total'],
                        $data['date_commande']
                    );
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        /**
         * Récupère toutes les commandes (sous forme de array(Commandes) d'un utilisateur
         * @param int $id_user
         * @return array()
         */
        public function readUser($id_user) {
            if($this->connexion == null) {
                return array();
            }
            else {
                $query = 'SELECT id_commandes, prix_total, date_commande FROM commandes WHERE id_users = :id ORDER BY id_commandes DESC';
                $preparedQuery = $this->connexion->prepare($query);
                $preparedQuery->bindParam(':id', $id_user, PDO::PARAM_INT);
                $preparedQuery->execute();
                return($preparedQuery->fetchAll(PDO::FETCH_ASSOC));
            }
        }

        /**
         * @return null|PDO
         */
        public function getConnexion()
        {
            return $this->connexion;
        }

        /**
         * @param null|PDO $connexion
         */
        public function setConnexion($connexion)
        {
            $this->connexion = $connexion;
        }

        /**
         * @return Commandes|null
         */
        public function getCommande()
        {
            return $this->commande;
        }

        /**
         * @param Commandes|null $commande
         */
        public function setCommande($commande)
        {
            $this->commande = $commande;
        }



    }
?>