<?php
    class Commandes {
        private $id_commandes;
        private $id_users;
        private $id_produits;
        private $prix_total;
        private $date_commande;
        private $qt_produits;

        /**
         * Commandes constructor.
         * @param int $id_commandes
         * @param int $id_users
         * @param string $id_produits
         * @param string $qt_produits
         * @param float $prix_total
         * @param string $date_commande

         */
        public function __construct($id_commandes = null, $id_users = null, $id_produits = null, $qt_produits = null, $prix_total = null, $date_commande = null)
        {
            $this->id_commandes = $id_commandes;
            $this->id_users = $id_users;
            $this->id_produits = $id_produits;
            $this->qt_produits = $qt_produits;
            $this->prix_total = $prix_total;
            $this->date_commande = $date_commande;
        }

        /**
         * Renvoie true si id_users, id_produits, qt_produits et prix_total sont != empty
         * @return bool
         */
        public function isRecordable() {
            return
                !empty($this->id_users)
                && !empty($this->id_produits)
                && !empty($this->qt_produits)
                && !empty($this->prix_total);
        }

        public function idProduitsToArray() {
            //echo('idProduits : ' . print_r($this->id_produits, true));
            return explode(',', $this->id_produits);
        }
        public function qtProduitsToArray() {
            //echo('qtProduits : ' . print_r($this->qt_produits, true));
            return explode(',', $this->qt_produits);
        }

        /**
         * @return int|null
         */
        public function getIdCommandes()
        {
            return $this->id_commandes;
        }

        /**
         * @param int|null $id_commandes
         */
        public function setIdCommandes($id_commandes)
        {
            $this->id_commandes = $id_commandes;
        }

        /**
         * @return int|null
         */
        public function getIdUsers()
        {
            return $this->id_users;
        }

        /**
         * @param int|null $id_users
         */
        public function setIdUsers($id_users)
        {
            $this->id_users = $id_users;
        }

        /**
         * @return string|null
         */
        public function getIdProduits()
        {
            return $this->id_produits;
        }

        /**
         * @param string|null $id_produits
         */
        public function setIdProduits($id_produits)
        {
            $this->id_produits = $id_produits;
        }

        /**
         * @return float|null
         */
        public function getPrixTotal()
        {
            return $this->prix_total;
        }

        /**
         * @param float|null $prix_total
         */
        public function setPrixTotal($prix_total)
        {
            $this->prix_total = $prix_total;
        }

        /**
         * @return null|string
         */
        public function getDateCommande()
        {
            return $this->date_commande;
        }

        /**
         * @param null|string $date_commande
         */
        public function setDateCommande($date_commande)
        {
            $this->date_commande = $date_commande;
        }

        /**
         * @return null|string
         */
        public function getQtProduits()
        {
            return $this->qt_produits;
        }

        /**
         * @param null|string $qt_produits
         */
        public function setQtProduits($qt_produits)
        {
            $this->qt_produits = $qt_produits;
        }
    }
?>