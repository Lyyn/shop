<?php

include_once('Cupcake.class.php');

class CupcakeDB {
    private $connexion;
    private $cupcake;
    /**
     * CupcakeDB constructor.
     * @param PDO $connexion
     * @param Cupcake $cupcake
     */
    public function __construct($connexion, $cupcake = null) {
        $this->connexion = $connexion;
        $this->cupcake = $cupcake;
    }


    /**
     * Retire $qt unité du cupcake $id
     * @param $id
     * @param $qt
     * @return bool
     */
    public function removeQt($id, $qt) {
        if(!empty($id) && !empty($qt)) {
            $query = 'UPDATE produits SET stock = stock - :qt WHERE id_produits = :id';
            $preparedQuery = $this->connexion->prepare($query);
            $preparedQuery->bindParam(':qt', $qt, PDO::PARAM_INT);
            $preparedQuery->bindParam(':id', $id, PDO::PARAM_INT);

            return(!$preparedQuery->execute());
        }
        return false;
    }

    public function read($id) {
        $query = 'SELECT * FROM produits WHERE id_produits = :id';
        $preparedQuery = $this->connexion->prepare($query);
        $preparedQuery->bindParam(':id', $id, PDO::PARAM_STR);

        if(!$preparedQuery->execute()) {
            return false;
        }
        else {
            $result = $preparedQuery->fetch(PDO::FETCH_ASSOC);

            $this->cupcake = new Cupcake(
                $this->connexion,
                $result['id_produits'],
                $result['id_categories'],
                $result['nom_produit'],
                $result['stock'],
                $result['prix'],
                $result['description']
            );

            return true;
        }
    }
    public function showCupcakeArray() {
        return $this->cupcake->toArray();
    }

    /**
     * @return Cupcake|null
     */
    public function getCupcake()
    {
        return $this->cupcake;
    }

    /**
     * @param Cupcake|null $cupcake
     */
    public function setCupcake($cupcake)
    {
        $this->cupcake = $cupcake;
    }
}
?>