<?php

class Cupcake {

    // Table produits
    private $id;            // ID du cupcake
    private $id_categorie;
    private $nom_produit;
    private $stock;
    private $prix;
    private $description;

    // Autres
    private $arrayPic;      // Toutes les images (sous forme de lien en string)
    private $connexion;     // Représente la connexion à la DB

    /**
     * Cupcake constructor.
     * @param PDO $connexion
     * @param int $id
     * @param int $id_categorie
     * @param string $nom_produit
     * @param int $stock
     * @param float $prix
     * @param string $description
     * @param array $arrayPic
     */
    public function __construct(
        $connexion,
        $id             = null,
        $id_categorie   = null,
        $nom_produit    = null,
        $stock          = null,
        $prix           = null,
        $description    = null,
        $arrayPic       = null
    )
    {
        $this->connexion    = $connexion;
        $this->id           = $id;
        $this->id_categorie = $id_categorie;
        $this->nom_produit  = $nom_produit;
        $this->stock        = $stock;
        $this->prix         = $prix;
        $this->description  = $description;
        $this->arrayPic     = $arrayPic;

        if($this->id != null) {
            $this->loadPics();
        }
    }

    public function loadPics() {
        if($this->id != null) {
            $query = 'SELECT "photo" FROM "photosProduits" WHERE id_produits = \''.$this->id.'\'';
            $resultSet = $this->connexion->query($query);
            $this->arrayPic = $resultSet->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function getPic() {
        if(!isset($this->arrayPic)) $this->loadPics();

        return $this->arrayPic[0]['photo'];
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getIdCategorie()
    {
        return $this->id_categorie;
    }

    /**
     * @param int|null $id_categorie
     */
    public function setIdCategorie($id_categorie)
    {
        $this->id_categorie = $id_categorie;
    }

    /**
     * @return null|string
     */
    public function getNomProduit()
    {
        return $this->nom_produit;
    }

    /**
     * @param null|string $nom_produit
     */
    public function setNomProduit($nom_produit)
    {
        $this->nom_produit = $nom_produit;
    }

    /**
     * @return int|null
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param int|null $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return float|null
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param float|null $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return array|null
     */
    public function getArrayPic()
    {
        return $this->arrayPic;
    }

    /**
     * @param array|null $arrayPic
     */
    public function setArrayPic($arrayPic)
    {
        $this->arrayPic = $arrayPic;
    }

    /**
     * @return PDO
     */
    public function getConnexion()
    {
        return $this->connexion;
    }

    /**
     * @param PDO $connexion
     */
    public function setConnexion($connexion)
    {
        $this->connexion = $connexion;
    }



    /**
     * The __toString method allows a class to decide how it will react when it is converted to a string.
     *
     * @return string
     * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.tostring
     */
    public function __toString()
    {
        $array = array();
        $array['id'] = $this->id;
        $array['id_categorie'] = $this->id_categorie;
        $array['nom_produit'] = $this->nom_produit;
        $array['stock'] = $this->stock;
        $array['prix'] = $this->prix;
        $array['description'] = $this->description;
        $array['arrayPic'] = $this->arrayPic;

        return print_r($this->toArray());
    }

    public function toArray() {
        $array = array();

        $array['id'] = $this->id;
        $array['id_categorie'] = $this->id_categorie;
        $array['nom_produit'] = $this->nom_produit;
        $array['stock'] = $this->stock;
        $array['prix'] = $this->prix;
        $array['description'] = $this->description;
        $array['arrayPic'] = $this->arrayPic;

        return $array;
    }

}
?>