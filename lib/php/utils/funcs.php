<?php

/**
 * @param Cupcake $cupCake
 */
function genCupFiche($cupCake) {

    if($cupCake != null) {
        $imgPath = 'lib/images/cupcakes/'.$cupCake->getPic();
        $desc = $cupCake->getDescription();
        $outStock = $cupCake->getStock() <= 0;

        if(strlen($desc) > 100) {
            $desc = substr($desc, 0, 115) . '...';
        }

        ?>
        <div class="cupFiche <?php echo($outStock ? 'grayscale' : '') ?>">
            <img class="img-responsive img-circle" src="<?php echo $imgPath; ?>"/>
            <h1><?php echo($cupCake->getNomProduit()); ?></h1>
            <p><?php echo($desc); ?></p>
            <p class="price"><?php echo $cupCake->getPrix().'€'; ?></p>
            <div style="text-align: center;">
                <button type="button" <?php echo($outStock ? 'disabled="true"' : ''); ?> class="btn btn-success cupcakeModal" data-id="<?php echo($cupCake->getId()); ?>"><span class="glyphicon glyphicon-info-sign"></span> <?php echo($outStock ? 'Hors stock' : 'Plus d\'infos'); ?></button>
            </div>
        </div>
    <?php
    }
}

/**
 * Vérifie si $_SESSION['state'] est plus grand ou égal au niveau indiqué
 * Renvoie true si oui
 * Renvoie false si pas de connexio ou niveau insuffisant
 * @param int $state
 * @return bool
 */
function checkState($state) {
    if(empty($_SESSION['state'])) {
        return false;
    }
    else {
        if($_SESSION['state'] < $state) {
            return false;
        }
        else {
            return true;
        }
    }
}

/**
 * Génère une liste de <option> avec les pays de la BDD
 * @param PDO $connexion
 */
function genListePays($connexion, $id_pays = null) {
    $query = 'SELECT id_pays, nom_pays FROM pays';
    $resultSet = $connexion->query($query);
    $result = $resultSet->fetchAll(PDO::FETCH_ASSOC);

    foreach($result as $r) {
        echo('<option '.(($r['id_pays'] == $id_pays) ? 'selected="selected"' : '').' value="'.$r['id_pays'].'">'.$r['nom_pays'].'</option>');
    }
}
?>