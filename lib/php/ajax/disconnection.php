<?php
    // Déconnexion
    session_start();        // D'abord récupérer les informations de connexion
    session_destroy();      // Détruire cette session
?>