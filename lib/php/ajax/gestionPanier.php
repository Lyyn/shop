<?php
    session_start();
    include_once('../../../admin/dbConnect.php');
    include_once('../classes/PanierDB.class.php');

    if(!empty($_GET['type'])) {
        switch($_GET['type']) {
            case 'delete':
                // $_GET['id'] -> id_paniers
                $panierDB = new PanierDB($connexion);
                if(!$panierDB->delete($_GET['id'])) {
                    echo('false');
                }
                else {
                    echo('true');
                }

                break;

            case 'add':
                // $_GET['id'] -> id_produits
                // $_GET['qt'] -> quantité

                if(empty($_SESSION['id_users'])) {
                    echo('false');
                }
                else {
                    $panierDB = new PanierDB($connexion);

                    if($panierDB->create($_SESSION['id_users'], $_GET['id'], $_GET['qt'])) {
                        echo('true');
                    }
                    else {
                        echo('false');
                    }
                }
                break;

            // Supprimer tout le panier d'un utilisateur
            case 'delFromUser':
                if(empty($_SESSION['id_users'])) {
                    echo('false');
                }
                else {
                    $panierDB = new PanierDB($connexion);

                    if($panierDB->delFromUser($_SESSION['id_users'])) {
                        echo('true');
                    }
                    else {
                        echo('false');
                    }
                }
                break;

            default:
                echo('false');
        }
    }
?>