<?php
    session_start();
    include_once('../../../admin/dbConnect.php');
    include_once('../classes/CupcakeDB.class.php');

    if(!empty($_GET['type'])) {
        switch($_GET['type']) {
            case 'remove':
                if(!empty($_GET['id']) && !empty($_GET['qt'])) {
                    $cupDB = new CupcakeDB($connexion);
                    $cupDB->removeQt($_GET['id'], $_GET['qt']);
                }
                break;

            default:
                echo('false');
        }
    }
?>