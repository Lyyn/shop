<?php
    include_once('../../../admin/dbConnect.php');

    // Traitement du modal login en AJAX
    // Les echo font office de return
    // "true"   : Connexion réussie
    // "false"  : raté
    if(!empty($_POST['type'])) {
        if($_POST['type'] == 'connexion') {
            $login  = $_POST['login'];
            $pass   = $_POST['password'];

            // Construction de la query
            $query = "select * from users where login = '".$login."' and pass = '".$pass."'";
            $resultSet = $connexion->query($query);

            if(!$resultSet) {
                echo "false : " . print_r($resultSet);
            }
            else {
                $data = $resultSet->fetch();
                if(!isset($data['login'])) {
                    echo "false";
                }
                else {
                    // Remplir les données de session
                    session_start();
                    //print_r($data);
                    $_SESSION['id_users']       = $data['id_users'];
                    $_SESSION['login']          = $data['login'];
                    $_SESSION['nom_affichage']  = $data['nom_affichage'];
                    $_SESSION['email']          = $data['email'];
                    $_SESSION['state']          = $data['state'];

                    //echo($_SESSION['state'] . ' ~ ' . $data['state']);

                    //echo "true\n" . print_r($data) . "\n" . print_r($resultSet);
                    echo "true";
                }
            }
        }
        // "true"   : inscription réussie
        // "login"  : login déjà existant
        // "mail"   : mail déjà existant
        // "false"  : autre erreur
        elseif($_POST['type'] == 'inscription') {
            $login  = $_POST['login'];
            $pass   = $_POST['password'];
            $mail   = $_POST['mail'];
            $exist  = false;

            // On vérifie que le login et le mail n'existent pas déjà
            $queryLogin = 'SELECT login FROM users WHERE login = \''.$login.'\'';
            $queryMail = 'SELECT email FROM users WHERE email = \''.$mail.'\'';

            $resultSet = $connexion->query($queryLogin);

            if($resultSet->fetch(PDO::FETCH_LAZY)) {
                echo "login";
                $exist = true;
            }
            else {
                $resultSet = $connexion->query($queryMail);

                if($resultSet->fetch(PDO::FETCH_LAZY)) {
                    echo "mail";
                    $exist = true;
                }
            }

            if(!$exist) {
                //$query = "INSERT INTO users (login, nom_affichage, pass, email, state) VALUES (?, ?, ?, ?, 0)";
                $query = 'INSERT INTO users (login, nom_affichage, pass, email, date_insc, state) VALUES (:login, :login, :pass, :mail, \''.date('d/m/Y').'\', 0)';
                $preparedQuery = $connexion->prepare($query);

                $preparedQuery->bindParam(':login', $login, PDO::PARAM_STR);
                $preparedQuery->bindParam(':pass', $pass, PDO::PARAM_STR);
                $preparedQuery->bindParam(':mail', $mail, PDO::PARAM_STR);

                $result = $preparedQuery->execute();

                if($result) {
                    echo "true";
                }
                else {
                    echo "false";
                }
            }

        }
        else {
            // Ne devrait jamais arriver
            echo "Ni inscription, ni connexion ?!";
        }
    }
    else {
        echo '$_POST == empty';
    }
?>