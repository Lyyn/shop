<?php
    header('Content-type: application/json');
    include_once('../classes/CupcakeDB.class.php');
    include_once('../../../admin/dbConnect.php');

    $json = null;

    if(!empty($_GET['id'])) {
        $cupcake = new CupcakeDB($connexion);
        if($cupcake->read($_GET['id'])) {
            echo json_encode($cupcake->getCupcake()->toArray());
        }
    }