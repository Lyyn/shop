<?php
    // Récupère simplement le nombre d'items dans le panier
    // Renvoie 'false' si erreur
    session_start();
    include_once('../../../admin/dbConnect.php');
    include_once('../classes/PanierDB.class.php');

    if(empty($_SESSION['id_users'])) {
        echo 'false';
    }
    else {
        $panier = new PanierDB($connexion);
        $panier->read($_SESSION['id_users']);
        echo $panier->getPanier()->getPanierSize();
    }
?>