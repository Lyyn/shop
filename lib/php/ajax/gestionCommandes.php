<?php
    session_start();
    include_once('../../../admin/dbConnect.php');
    include_once('../classes/CommandesDB.class.php');

    if(!empty($_POST['type'])) {
        switch($_POST['type']) {
            case 'add':
                if(empty($_SESSION['id_users'])) {
                    echo('false');
                }
                else {
                    $commande = new Commandes(
                        null,
                        $_SESSION['id_users'],
                        $_POST['idProduits'],
                        $_POST['qtProduits'],
                        $_POST['prixTotal']
                    );
                    $commandeDB = new CommandesDB($connexion);
                    if($commandeDB->create($commande)) {
                        echo('true');
                    }
                    else {
                        echo('false');
                    }
                }
                break;

            default:
                echo('false');
        }
    }

?>