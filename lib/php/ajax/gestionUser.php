<?php
    include_once('../../../admin/dbConnect.php');
    include_once('../classes/UsersDB.class.php');
    session_start();

    if(!empty($_POST)) {
        switch($_POST['type']) {

            // Vérifier si l'user peut effectuer une commande
            case 'peutCommander':
                if(!empty($_SESSION['id_users'])) {
                    $user = new UsersDB($connexion);
                    if($user->read($_SESSION['id_users'])) {
                        if($user->peutCommander()) {
                            echo('true');
                        }
                        else {
                            echo('false');
                        }
                    }
                }
                break;

            case 'update':
                if(!empty($_SESSION['id_users'])) {
                    $query = '
                        UPDATE users SET
                        nom_affichage = :nomaff,
                        email = :email
                    ';

                    // Parties facultatives
                    if(!empty($_POST['adresse']))       $query .= ', adresse = :adresse';
                    if(!empty($_POST['codePostal']))    $query .= ', code_postal = :cp';
                    if(!empty($_POST['idPays']))        $query .= ', id_pays = :pays';

                    if(empty($_POST['adresse']))       $query .= ', adresse = NULL';
                    if(empty($_POST['codePostal']))    $query .= ', code_postal = NULL';
                    if(empty($_POST['idPays']))        $query .= ', id_pays = NULL';

                    // LE PLUS IMPORTANT.
                    $query .= ' WHERE id_users = :idUser';

                    // Ajout des paramètres
                    $preparedQuery = $connexion->prepare($query);
                    $preparedQuery->bindParam(':nomaff', $_POST['nom_affichage'], PDO::PARAM_STR);
                    $preparedQuery->bindParam(':email', $_POST['email'], PDO::PARAM_STR);
                    if(!empty($_POST['adresse']))       $preparedQuery->bindParam(':adresse', $_POST['adresse'], PDO::PARAM_STR);
                    if(!empty($_POST['codePostal']))    $preparedQuery->bindParam(':cp', $_POST['codePostal'], PDO::PARAM_STR);
                    if(!empty($_POST['idPays']))        $preparedQuery->bindParam(':pays', $_POST['idPays'], PDO::PARAM_INT);
                    $preparedQuery->bindParam(':idUser', $_SESSION['id_users'], PDO::PARAM_INT);

                    $result = $preparedQuery->execute();
                    if(!$result) {
                        echo('false');
                    }
                    else {
                        $_SESSION['nom_affichage'] = $_POST['nom_affichage'];
                        echo('true');
                    }
                }
                else {
                    echo('');
                }

                break;

            default:
                echo('false');
        }
    }
?>