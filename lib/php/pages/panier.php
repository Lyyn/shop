<?php
    // Page du panier de l'utilisateur

    echo('<h1>Mon panier</h1>');

    $panierVide = $panier->getPanier()->getPanierSize() == 0;

    echo('<div id="panierVide" style="display: '.(($panierVide) ? 'block' : 'none' ).'"><p>Votre panier ne contient aucun élément pour le moment.</p><p>Pourquoi ne pas commencer <a href="?p=shop">vos achats</a> dès maintenant ?</p></div>');

    $totalPrice = 0;
    //echo('<pre>'.print_r($panier->getPanier()->getCupcakes(), true).'</pre>');
    ?>
    <div id="panierNonVide" style="display: <?php echo((($panierVide) ? 'none' : 'block' )); ?>">
        <p>Retrouvez ici les différents cupcakes ajoutés à votre panier au cours de vos achats. Vérifiez ce que vous avez sélectionné et, ensuite, passez votre commande en cliquant sur le bouton en bas à droite.</p>
        <p><b>Attention !</b> Les éléments dans votre panier ne sont pas réservés, si une autre personne passe une commande et vide entièrement le lot d'un type de cupcake, il est malheureusement trop tard pour vous.</b></p>
        <div class="table-responsive" id="tablePanier">
            <table class="table table-striped">
                <tr>
                    <th>Photo</th>
                    <th>Nom du produit</th>
                    <th>Quantité</th>
                    <th>Prix unitaire</th>
                    <th>Prix final</th>
                    <th>Supprimer ?</th>
                </tr>
                <?php
                    //$i = 0;
                    foreach($panier->getPanier()->getCupcakes() as $cupcakeDB) {
                        // getCupcakes -> array(id_paniers, cupcake (cupcakeDB), qt)
                        $cupcake = $cupcakeDB['cupcake']->getCupcake();
                        $qt = $cupcakeDB['quantite'];
                        $id = $cupcakeDB['id_paniers'];
                        $idType = $cupcake->getId();
                        $prix = $cupcake->getPrix();
                        $prixF = $prix * $qt;
                        $totalPrice += $prixF;

                        // data-type="'.$idType.'
                        echo('<tr data-id="'.$id.'">');
                        echo('
                            <td><img class="img-responsive img-circle" src="lib/images/cupcakes/'.$cupcake->getPic().'"/></td>
                            <td>'.$cupcake->getNomProduit().'</td>
                            <td>'.$qt.' unités</td>
                            <td>'.$prix.'€</td>
                            <td data-id="'.$id.'">'.$prixF.'€</td>
                            <td><a href="" data-id="'.$id.'"><span class="glyphicon glyphicon-remove"></span></a></td>
                            <td style="display: none;">'.$idType.'</td>
                        ');
                        echo('</tr>');
                    }
                ?>
            </table>
        </div>
        <span class="priceText">Prix final (TTC) : </span><span class="price" id="priceNumber"><?php echo($totalPrice); ?>€</span>
        <button type="button" id="passerCommande" class="btn btn-success pull-right"><span class="glyphicon glyphicon-shopping-cart"></span> Passer la commande</button>
    </div>

