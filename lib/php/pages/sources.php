
<h1>Sources des images</h1>
    <ul>
        <li>Bannière : <a href="https://bakedcupcakery.wordpress.com/tag/christmas-tree-cupcakes/" target="_blank">https://bakedcupcakery.wordpress.com/tag/christmas-tree-cupcakes/</a></li>
        <li>Cupcake footer : <a href="http://ireneshomemadecreations.com/cupcakes.html" target="_blank">http://ireneshomemadecreations.com/cupcakes.html</a></li>
        <li>Cupcake 1 : <a href="http://www.icons101.com/icon/id_41309/setid_1221/Chocolate_Obsession_by_Artbees/CupCake256" target="_blank">http://www.icons101.com/icon/id_41309/setid_1221/Chocolate_Obsession_by_Artbees/CupCake256</a></li>
        <li>Cupcake 2 : <a href="http://findicons.com/icon/72840/cherry_chocolate_2" target="_blank">http://findicons.com/icon/72840/cherry_chocolate_2</a></li>
        <li>Cupcake 3 : <a href="http://findicons.com/icon/72827/cupcakes_cherry_pink" target="_blank">http://findicons.com/icon/72827/cupcakes_cherry_pink</a></li>
        <li>Cupcake 4 : <a href="http://colorcupcake.com/" target="_blank">http://colorcupcake.com/</a></li>
        <li>Cupcake 5 : <a href="http://khayilsbakeshop.com/flavors/" target="_blank">http://khayilsbakeshop.com/flavors/</a></li>
        <li>Cupcake 6 : <a href="http://trove.wikia.com/wiki/Wild_Cupcake" target="_blank">http://trove.wikia.com/wiki/Wild_Cupcake</a></li>
        <li>Petit chaton mignon : <a href="http://www.dose.ca/2013/10/07/top-10-cutest-kitten-gifs-ever-yes-we-just-opened-pandoras-box" target="_blank">http://www.dose.ca/2013/10/07/top-10-cutest-kitten-gifs-ever-yes-we-just-opened-pandoras-box</a></li>
        <li>Cupcake de l'accueil : <a href="http://thatguy0365.tumblr.com/post/123645793467/so-beautiful" target="_blank">http://thatguy0365.tumblr.com/post/123645793467/so-beautiful</a></li>
        <li>Dawson (de la page d'accueil> : <a href="http://www.helloloser.com/2012/05/19/laughing-big-man/" target="_blank">http://www.helloloser.com/2012/05/19/laughing-big-man/</a></li>
    </ul>
<p>Certaines images ont pu être retouchées par mes soins.</p>

<h2>Remerciements</h2>
<p><a href="http://twitter.com/Hikasawr" target="_blank">@Hikasawr</a> pour l'idée des cupcakes, <a href="http://twitter.com/Zukill_" target="_blank">@Zukill_</a> pour le nom du site.</p>
