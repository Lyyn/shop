<!-- Modal login/inscription -->

<div class="modal fade" id="modal_login" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <a href="#" class="close" id="modal_login_close" data-dismiss="modal">&times;</a> <!-- Bouton pour fermer le modal -->
                </div>
                <h1 class="normal" id="modal_login_title">Connexion</h1>
            </div>

            <form role="form">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="pseudo" data-toggle="tooltip" data-placement="right" data-html="true" title="Doit contenir entre 4 et 40 caractères.<br/>Ne peut pas contenir de caractères spéciaux autres que : _ - +"><span class="glyphicon glyphicon-user"></span> Nom d'utilisateur :</label>
                        <p id="hintPseudo" class="hint"></p>
                        <input type="text" id="pseudo" class="form-control" placeholder="Entrez votre nom d'utilisateur"/>
                    </div>

                    <div class="form-group">
                        <label for="mdp" data-toggle="tooltip" data-placement="right" data-html="true" title="Doit contenir entre 6 et 40 caractères.<br/>Caractères spéciaux acceptés et conseillés."><span class="glyphicon glyphicon-eye-close"></span> Mot de passe :</label>
                        <p id="hintMdp" class="hint"></p>
                        <input type="password" id="mdp" class="form-control" placeholder="••••••••"/>
                    </div>


                    <!-- sera affiché uniquement si on s'inscrit -->
                    <div class="form-group login_hidden">
                        <label for="mdp2" data-toggle="tooltip" data-placement="right" data-html="true" title="Doit contenir entre 6 et 40 caractères.<br/>Caractères spéciaux acceptés et conseillés."><span class="glyphicon glyphicon-eye-close"></span> Mot de passe (répéter) :</label>
                        <p id="hintMdp2" class="hint"></p>
                        <input type="password" id="mdp2" class="form-control" placeholder="••••••••"/>
                    </div>

                    <div class="form-group login_hidden">
                        <label for="email" data-toggle="tooltip" data-placement="right" data-html="true" title="Doit être une adresse e-mail syntaxiquement correcte."><span class="glyphicon glyphicon-envelope"></span> E-mail :</label>
                        <p id="hintMail" class="hint"></p>
                        <input type="email" id="email" class="form-control" placeholder="cup@ca.ke"/>
                    </div>
                    
                    <div class="checkbox">
                        <label><input type="checkbox" value="" id="newUser"> Nouvel utilisateur ?</label>
                    </div>
                </div>
                <div class="modal-footer">
                        <button type="submit" id="submitLogin" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok"></span> Valider</button>
                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Annuler</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal déconnexion -->
<div class="modal fade" id="modal_logoff" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <a href="#" class="close" id="modal_logoff_close" data-dismiss="modal">&times;</a> <!-- Bouton pour fermer le modal -->
                </div>
                <h1 class="normal" id="modal_logoff_title">Déconnexion</h1>
            </div>
            <div class="modal-body" id="modal_logoff_content">
                <p>Êtes-vous sûr(e) de vouloir vous déconnecter ?</p> <p>... Peut-être est-ce juste un clic mal placé ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success pull-right" id="modalDeconnexion"><span class="glyphicon glyphicon-ok"></span> Confirmer</button>
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Annuler</button>
            </div>
        </div>
    </div>
</div>



<!-- Modal Info (template) -->

<div class="modal fade" id="modal_info" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <a href="#" class="close" id="modal_info_close" data-dismiss="modal">&times;</a> <!-- Bouton pour fermer le modal -->
                </div>
                <h1 class="normal" id="modal_info_title">Information</h1>
            </div>
            <div class="modal-body" id="modal_info_content">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Ok</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal question (template) -->
<div class="modal fade" id="modal_question" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <a href="#" class="close" id="modal_question_close" data-dismiss="modal">&times;</a> <!-- Bouton pour fermer le modal -->
                </div>
                <h1 class="normal" id="modal_question_title"></h1>
            </div>
            <div class="modal-body" id="modal_question_content">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Annuler</button>
                <button type="button" id="modalQuestion" class="btn btn-success pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Confirmer</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Cupcake info (template) -->
<div class="modal fade" id="modal_cupcake" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <a href="#" class="close" id="modal_cupcake_close" data-dismiss="modal">&times;</a> <!-- Bouton pour fermer le modal -->
                </div>
                <h1 class="normal" id="modal_cupcake_title"></h1>
            </div>
            <div class="modal-body" id="modal_cupcake_content">
                <div style="display: inline-block; width: 100%; height: 100%;">
                    <div style="text-align: center;"><img src="" id="imageCupcake" class="col-md-4 img-circle img-responsive"/></div>
                    <div class="col-md-8">
                        <div id="descCupcake"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="decompteCupcake">En stock : <span class="number"></span></div>
                <button type="button" id="panierCupcake" class="btn btn-info pull-right"><span class="glyphicon glyphicon-shopping-cart"></span> Ajouter au panier</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal ajout panier (nombre) -->
<div class="modal fade" id="modal_ajoutPanier" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div>
                    <a href="#" class="close" id="modal_ajoutPanier_close" data-dismiss="modal">&times;</a> <!-- Bouton pour fermer le modal -->
                </div>
                <h1 class="normal" id="modal_ajoutPanier_title">Ajouter au panier</h1>
            </div>
            <div class="modal-body" id="modal_ajoutPanier_content">
                <p class="hint">Ceci est une erreur. Une grave erreur, bordel.</p>
                <p>Combien de cupcake de ce type voulez-vous ajouter à votre panier ?</p>
                <div class="input-group">
                    <input type="number" class="form-control" id="numberCupcake" value="1"/>
                    <div class="input-group-addon">unités</div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="decompteCupcake">En stock : <span class="number"></span></div>
                <button type="button" id="modalQtCup" class="btn btn-success pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Confirmer</button>
            </div>
        </div>
    </div>
</div>