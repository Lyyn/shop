<?php
    // Page d'administration / tests, on ne laisse personne y venir sans un state 42
    include_once('lib/php/classes/Panier.class.php');
    include_once('lib/php/classes/Cupcake.class.php');
    include_once('lib/php/classes/CupcakeDB.class.php');
    include_once('lib/php/classes/PanierDB.class.php');
    include_once('lib/php/utils/funcs.php');
    $showPage = false;

    if(checkState(42)) $showPage = true;

    if(!$showPage) {
        include('404.php');
    }
    else {
        ?>

        <h1>Section admin (Les tests)</h1>
        <p><a href="genPDF.php?id=1" target="_blank">Test PDF</a></p>

        <?php
            //$panier = new Panier($connexion);
            for($i = 1; $i < 7; $i++) {
                $cupDB = new CupcakeDB($connexion);

                if($cupDB->read($i)) {
                    // true = read ok
                    //echo('<pre>');
                    //print_r($cupDB->showCupcakeArray());
                    //print_r($cupDB->getCupcake());
                    //print_r($cupDB->getCupcake()->getNomProduit());
                    //echo('</pre>');

                    genCupFiche($cupDB->getCupcake());
                }
                else {
                    echo('??');
                }
            }
        /*
        $foo = new PanierDB($connexion);

        if(!isset($_SESSION['panier'])) {
            $_SESSION['panier'] = $foo;
            echo('Panier session créé');
        }
        else {
            echo('Panier existant :<pre>');
            print_r($_SESSION['panier']);
            echo('</pre>');
        }
        */
        //echo('<pre>');
            //$foo->read($_SESSION['id_users']);
            //echo('</pre>');
    }

?>

