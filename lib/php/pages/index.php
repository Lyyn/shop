<h1>Bienvenue sur TopKek !</h1>
<img class="img-circle img-responsive imgContenu" src="lib/images/cupindex.png"/>
<h2>Les meilleurs cupcakes...</h2>
<p>... sont enfin à portée de main <em>- ou devrions-nous dire à portée de clic ! -</em>. Découvrez <a href="?p=shop">nos offres</a> <b>originales</b> et <b>bon marché</b>.</p>
<img class="img-circle img-responsive imgContenu" src="lib/images/cupindex2.png"/>
<h2>Satisfait ET remboursé !</h2>
<p>Si par malheur, un de nos produits ne vous convenait pas, vous avez la possibilité de demander un remboursement de cet argent malheureusement perdu pour quelque chose qui ne vous a pas satisfait. Car votre <b>satisfaction</b>, quelle vienne de nos produits ou de notre service est notre <b>priorité</b> !</p>
<p><em>Offre soumise à conditions</em></p>
<img class="img-circle img-responsive imgContenu" src="lib/images/cupindex3.png"/>
<h2>Une suggestion ?</h2>
<p>Tous nos produits sont confectionnés artisinalement, avec amour. Vous voulez nous aider à étayer notre offre avec une superbe idée ? N'hésitez surtout pas à nous contacter ! Il se pourrait même que vous y gagniez quelque chose...</p>
