<?php
    include_once('lib/php/classes/UsersDB.class.php');
    include_once('lib/php/classes/CommandesDB.class.php');

    $showPage = true;
    if(!isset($_SESSION['id_users'])) {
        $showPage = false;
    }
    else {
        $user = new UsersDB($connexion);
        $user->read($_SESSION['id_users']);
        $donnees = $user->getUser()->getArray();
    }

if($showPage) {

    ?>
    <h1>Mon compte</h1>
    <p>Sur cette page, vous pouvez ajouter ou rectifier vos informations personnelles.</p><p><b>Attention !</b> Votre adresse, code postal et pays sont requis avant de valider toute commande !</p>
    <form class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label">Login :</label>
            <div class="col-sm-9">
                <input type="text" disabled class="form-control" placeholder="<?php echo($donnees['login']); ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Date d'inscription :</label>
            <div class="col-sm-9">
                <input type="text" disabled class="form-control" placeholder="<?php echo($donnees['date_insc']) ?>"/>
            </div>
        </div>
        <div class="form-group">
            <p class="hint alRight" id="hintCompteNomAffichage"></p>
            <label class="col-sm-3 control-label">Nom d'affichage :</label>
            <div class="col-sm-9">
                <input type="text" id="compte_nom_affichage" class="form-control" placeholder="Entrez votre nom d'affichage ici..." value="<?php echo($donnees['nom_affichage']); ?>"/>
            </div>
        </div>
        <div class="form-group">
            <p class="hint" id="hintCompteEmail"></p>
            <label class="col-sm-3 control-label">E-mail :</label>
            <div class="col-sm-9">
                <input type="text" id="compte_email" class="form-control" placeholder="Entrez votre adresse e-mail ici..." value="<?php echo($donnees['email']); ?>"/>
            </div>
        </div>
        <div class="form-group">
            <p class="hint" id="hintCompteAdresse"></p>
            <label class="col-sm-3 control-label">Adresse :</label>
            <div class="col-sm-9">
                <input type="text" id="compte_adresse" class="form-control" placeholder="Entrez votre adresse ici..." value="<?php echo($donnees['adresse']); ?>"/>
            </div>
        </div>
        <div class="form-group">
            <p class="hint" id="hintCompteCodePostal"></p>
            <label class="col-sm-3 control-label">Code postal :</label>
            <div class="col-sm-9">
                <input type="text" id="compte_code_postal" class="form-control" placeholder="Entrez votre code-postal ici..." value="<?php echo($donnees['code_postal']); ?>"/>
            </div>
        </div>
        <div class="form-group">
            <p class="hint" id="hintComptePays"></p>
            <label class="col-sm-3 control-label">Votre pays :</label>
            <div class="col-sm-9">
                <select class="form-control combobox" id="compte_pays">
                    <option></option>
                    <?php genListePays($connexion, $donnees['id_pays']); ?>
                </select>
            </div>
        </div>
        <button id="validCompte" class="btn btn-success" style="display: block; margin-left: auto;"><span class="glyphicon glyphicon-ok"></span>  Valider</button>
    </form>

    <h1>Mes commandes</h1>
    <?php
        $commandesDB = new CommandesDB($connexion);
        $commandes = $commandesDB->readUser($_SESSION['id_users']); // id_commandes, prix_total, date_commande

        // array_count_values ne peut pas compter une array d'array...
        if(!empty($commandes)) {
            ?>
            <table class="table table-condensed table-striped table-responsive">
                <tr>
                    <th>ID</th>
                    <th>Date de commande</th>
                    <th>Prix total</th>
                    <th>Facture</th>
                </tr>
                <?php
                    foreach($commandes as $row) {
                        echo('<tr>');
                        echo('<td>'.$row['id_commandes'].'</td>');
                        echo('<td>'.$row['date_commande'].'</td>');
                        echo('<td>'.$row['prix_total'].' €</td>');
                        echo('<td><a href="genPDF.php?id='.$row['id_commandes'].'"><span class="glyphicon glyphicon-sunglasses"></span></a></td>');
                        echo('</tr>');
                    }
                ?>
            </table>
            <?php
        }
    else {
        echo('<p>Vous n\'avez pas encore passé de commande, quel dommage ! Pourquoi ne pas <a href="?p=shop">commencer</a> dès maintenant ?</p>');
    }
}
else {
    include('404.php');
}
?>
