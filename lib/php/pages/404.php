<h1>Page non trouvée</h1>
<p>Nous avons cherché partout. Dans tous les dossiers. Sous la table. Dans les tiroirs. Impossible de mettre la main sur cette page que vous désiriez...</p>
<p>Il est fort possible que l'erreur soit de notre ressort, mais pourriez-vous, s'il-vous-plaît, vérifier que vous n'en avez pas fait une de votre côté... S'il-vous-plaît ?</p>
<center><img src="lib/images/404.jpg" alt="Un petit chaton vraiment mignon"/></center>