
<h1>Nos cupcakes</h1>
<p>Retrouvez ici nos différents cupcakes, tous cuisinés avec amour et passion !</p>
<?php
    for($i = 1; $i < 7; $i++) {
        $cupDB = new CupcakeDB($connexion);

        if($cupDB->read($i)) {
            genCupFiche($cupDB->getCupcake());
        }
    }
?>