$(document).ready(function(){
    // Règle un bug de bootstrap avec les modals qui décalent le <body> à cause de la scrollbar
    // Source : http://stackoverflow.com/a/22267856/2455658
    $(document.body)
        .on('show.bs.modal', function () {
            if (this.clientHeight <= window.innerHeight) {
                return;
            }
            // Get scrollbar width
            var scrollbarWidth = getScrollBarWidth();
            if (scrollbarWidth) {
                $(document.body).css('padding-right', scrollbarWidth);
                $('.navbar-fixed-top').css('padding-right', scrollbarWidth);
            }
        })
        .on('hidden.bs.modal', function () {
            $(document.body).css('padding-right', 0);
            $('.navbar-fixed-top').css('padding-right', 0);
        });

    function getScrollBarWidth () {
        var inner = document.createElement('p');
        inner.style.width = "100%";
        inner.style.height = "200px";

        var outer = document.createElement('div');
        outer.style.position = "absolute";
        outer.style.top = "0px";
        outer.style.left = "0px";
        outer.style.visibility = "hidden";
        outer.style.width = "200px";
        outer.style.height = "150px";
        outer.style.overflow = "hidden";
        outer.appendChild (inner);

        document.body.appendChild (outer);
        var w1 = inner.offsetWidth;
        outer.style.overflow = 'scroll';
        var w2 = inner.offsetWidth;
        if (w1 == w2) w2 = outer.clientWidth;

        document.body.removeChild (outer);

        return (w1 - w2);

    }


    // Activer tous les tooltips existants
    $('[data-toggle="tooltip"]').tooltip();


    /*
        LOGIN MODAL
    */

    // La checkbox affichera ou non les champs + changement titre
    $('#newUser').click(function () {
        var $title = $('#modal_login_title');
        
        if ($('#newUser').is(':checked')) {
            $('.login_hidden').css('display', 'block');
            $title.text('Inscription');
            $title.attr('class', 'inverted');
        }
        else {
            $('.login_hidden').css('display', 'none');
            $title.text('Connexion');
            $title.attr('class', 'normal');
        }
    });
    
    // Fermer le modal à l'aide de la petite croix (qui est un <a>, pas un bouton cette fois)
    $('#modal_login_close').click(function() {
        $('#modal_login').modal('hide');
    });
    
    // Modifier le bouton submit
    $('#submitLogin').click(function(e) {
        // Flags
        var formValid  = true;
        var content = '';

        // Objets du DOM
        var $hintPseudo = $('#hintPseudo');
        var $hintMdp = $('#hintMdp');
        var $hintMdp2 = $('#hintMdp2');
        var $hintMail = $('#hintMail');
        
        // Clear les hints
        $hintPseudo.html('');
        $hintMdp.html('');
        $hintMdp2.html('');
        $hintMail.html('');
        
        // Valeurs du formulaire
        var pseudo      = $('#pseudo').val();
        var mdp         = $('#mdp').val();
        var mdp2        = $('#mdp2').val();
        var email       = $('#email').val();
        
        // Regex
        var regPseudo = /^[\w _\-\+]{4,40}$/ig;         // tous les caractères A-Z, espace, car spéciaux, entre 4 et 40 caractères max
        var regMail = /([\w_+\.]{1,})@([\w]){2,}\.([\w]){2,}/ig; // Flemme de décrire
        //var regMdp = /^.{6,40}$/ig;                    // tous les caractères, 6 min 40 max
        
        
        e.preventDefault(); // Enlever les fonctions par défaut du bouton submit (tout sera géré en AJAX)
        
        // Checks basiques
        if(pseudo.length < 4 || pseudo.length > 40) {
            $hintPseudo.html('Votre nom doit contenir entre 4 et 40 caractères.');
            $hintPseudo.css('display', 'block');
            formValid = false;
            
        }
        
        if(mdp.length < 6 || mdp.length > 40) {
            $hintMdp.html('Votre mot de passe doit contenir entre 6 et 40 caractères.');
            $hintMdp.css('display', 'block');
            formValid = false;
        }
        
        // Checks plus avancés (regex)
        if(!regPseudo.test(pseudo) && pseudo.length > 0) {
            $hintPseudo.html($hintPseudo.html() + '<br/> Votre nom contient des caractères invalides.');
            $hintPseudo.css('display', 'block');
            formValid = false;
        }

        // Connexion ou inscription ?
        if(!$('#newUser').is(':checked') && formValid) {
            // Connexion
            doLogin(pseudo, mdp);
        }
        else {
            // Inscription demandée, faire tous les checks supplémentaires nécessaires (les premiers ont été faits)
            var secondCheck = true;

            // Le deuxième champs de mdp doit avoir entre 6 et 40 caractères
            if(mdp2.length < 6 || mdp2.length > 40) {
                $hintMdp2.html('Votre mot de passe doit contenir entre 6 et 40 caractères.');
                $hintMdp2.css('display', 'block');
                secondCheck = false;
            }

            // Les deux champs de mdp doivent être identiques
            if(mdp2 != mdp) {
                $hintMdp2.html($hintMdp.html().concat((($hintMdp2.html().length > 0) ? '<br/>' : '').concat('<p>Les champs des mots de passe doivent être identiques.</p>')));
                $hintMdp2.css('display', 'block');
                secondCheck = false;
            }

            // L'email doit répondre à la regex
            if(!regMail.test(email)) {
                // L'email ne passe pas la regex
                $hintMail.css('display', 'block');
                $hintMail.html('<p>La syntaxe de votre e-mail n\'est pas correcte.</p>');
                secondCheck = false;
            }

            // Tout est correct
            if(formValid && secondCheck) {
                $.post(
                    'lib/php/ajax/login.php',
                    {
                        login : pseudo,
                        password : mdp,
                        mail : email,
                        type : 'inscription'      // Renvoie "false" si raté, "true" si réussi
                    },
                    function(result) {
                        // retours possibles : true, login, mail, false

                        switch(result) {
                            case 'login':
                                $hintPseudo.html('Votre nom d\'utilisateur est déjà pris, veuillez en utiliser un autre.');
                                $hintPseudo.css('display', 'block');
                                break;

                            case 'mail':
                                $hintMail.html('Cette adresse e-mail est déjà utilisée, veuillez en utiliser une autre.');
                                $hintMail.css('display', 'block');
                                break;

                            case 'true':
                                $('#modal_login').modal('hide');
                                content = '<p>Vous êtes à présent inscrit sur notre site ! Bienvenue parmi nous !</p><p>Ce site ne requiert pas d\'activation par mail, vous êtes donc d\'ores et déjà connecté.</p>';
                                modalInfo(content);

                                // On se connecte direct au compte créé (en mode silencieux)
                                doSilentLogin(pseudo, mdp);
                                clearModalLogin();
                                break;

                            case 'false':
                            default:
                                content = '<h2>Une erreur est survenue lors de l\'inscription.</h2><p>Veuillez réessayer plus tard...</p>';

                                //if(result != 'false') content.concat('<p>'.concat(result).concat('</p>'));
                                alert(result);

                                modalInfo(content);
                                break;
                        }
                    },
                    'text'
                );
            }
        }
    });

    $('#modalDeconnexion').click(function() {
        // L'utilisateur a confirmé vouloir se déconnecter
        $.get(
            'lib/php/ajax/disconnection.php',
            null,
            function() {
                $('#loginMenu').html('Anonyme - <a href="" id="loginLink" data-toggle="modal" data-target="#modal_login">Se connecter</a>');
                $('#headerPanier').css('display', 'none');
                $('#modal_logoff').modal('hide');
                $('#menuAdmin').remove();

                var content = '<h2>À bientôt !</h2><p>Vous êtes à présent déconnecté(e) de ce site web. Nous espérons vous revoir très bientôt !</p>';
                modalInfo(content);

            },
            null
        )
    });

    /*
     * Permet de clear complètement le formulaire de login
     */
    function clearModalLogin() {
        $('#pseudo').val('');
        $('#mdp').val('');
        $('#mdp2').val('');
        $('#email').val('');
        $('#hintPseudo').css('display', 'none');
        $('#hintMdp').css('display', 'none');
        $('#hintMdp2').css('display', 'none');
        $('#hintMail').css('display', 'none');
    }

    /*
     * Permet d'afficher le modal d'info
     * content : le contenu du modal
     * title : titre (par défaut : information)
     * showModal : si != null, réafficher le modal indiqué
     * reload : si != null, redirige la page vers l'accueil à la fermeture du modal
     */
    function modalInfoShow(content, title, modalToHide) {
        var infoModal = $('#modal_info');
        if(title == null) {
            title = 'Information';
        }

        $('#modal_info_content').html(content);
        $('#modal_info_title').html(title);

        if(modalToHide != null) {
            modalToHide.modal('hide');
            infoModal.on('hidden.bs.modal', function() {
                modalToHide.modal('show');
                infoModal.off('hidden.bs.modal', null);
            });
        }

        infoModal.modal('show');
    }
    function modalInfo(content) {
        modalInfoShow(content, null, null);
    }


    // LOGIN
    function doLogin(pseudo, mdp) {
        doTrueLogin(pseudo, mdp, false);
    }
    function doSilentLogin(pseudo, mdp) {
        doTrueLogin(pseudo, mdp, true);
    }
    function doTrueLogin(pseudo, mdp, silent) {
        var content = '';
        $.post(
            'lib/php/ajax/login.php',
            {
                login : pseudo,
                password : mdp,
                type : 'connexion'      // Renvoie "false" si raté, "true" si réussi
            },
            function(result) {
                if(result == 'true') {
                    // connexion réussie
                    $('#modal_login').modal('hide');

                    if(!silent) {
                        content = '<h2>Bienvenue !</h2><p>Vous êtes à présent bien connecté !';
                        modalInfo(content);
                    }

                    // On va récupérer les infos de session pour changer le menu et afficher le lien de déconnexion + le nom d'affichage
                    // Il peut y avoir une erreur, le script renverra une chaîne vide si c'est le cas (sinon juste le nom)
                    $.get(
                        'lib/php/ajax/getSessionInfo.php',
                        {
                            type: 'nom_affichage'
                        },
                        function(result) {
                            if(result == '') {
                                // Rien dans $_SESSION, mais ça ne devrait pas arriver.
                                $('#modal_login').modal('hide');
                                content = '<h2>Une erreur inconnue est survenue lors de la connexion.</h2><p>Veuillez essayer de vous reconnecter</p>';
                                modalInfo(content);
                            }
                            else {
                                $('#loginMenu').html('<a href="?p=compte">' + result + '</a> - <a href="" id="loginLink" data-toggle="modal" data-target="#modal_logoff">Se déconnecter</a>');
                            }
                        },
                        'text'
                    );

                    // Ajout du panier (récupération du nombre)
                    $.get(
                        'lib/php/ajax/getPanierInfo.php',
                        null,
                        function(result) {
                            if(result != 'false') {
                                $('#headerPanier').css('display', 'inline');
                                $('#headerPanier .badge').text(' ' + result);
                            }
                        },
                        'text'
                    );

                    $.get(
                        'lib/php/ajax/getSessionInfo.php',
                        {
                            type: 'state'
                        },
                        function(result) {
                            if(result == 42) {
                                $('#menu ul').html($('#menu ul').html().concat('<li class="admin" id="menuAdmin"><a href="?p=admCake">Admin</a></li>'));
                            }
                        },
                        'text'
                    );

                    if(!silent) {
                        clearModalLogin();
                    }

                }
                else {
                    // connexion ratée
                    content = '<h2>Une erreur est survenue lors de la connexion.</h2><p>Êtes-vous sûr d\'avoir entré les bons identifiants ?</p>';
                    modalInfoShow(content, null, $('#modal_login'));
                }
            },
            'text'
        );

    }

    /*
        CUPCAKE MODAL (Magasin)
     */
    $('.cupcakeModal').click(function() {
        if(this.dataset['id'] != null) {
            $.get(
                'lib/php/ajax/getCupcakeInfo.php',
                {
                    id: this.dataset['id']
                },
                function(result) {
                    // Result = objet JSON
                    console.log(result);
                    if(result != null) {
                        // Objets utiles
                        var $content = $('#descCupcake');
                        var stock = result.stock;
                        var $number = $('.number');


                        // Modifier l'apparence du modal pour correspondre au cupcake choisi
                        $('#modal_cupcake_title').text(result.nom_produit);
                        $('#imageCupcake').attr('src', 'lib/images/cupcakes/' + result.arrayPic[0].photo);
                        $content.html('<p>' + result.description + '</p>');
                        $content.html($content.html().concat('<p class="price">' + result.prix + '€</p>'));

                        // Nombre du stock en rouge si stock faible
                        $number.removeClass('rouge vert');
                        if(stock <= 10) {
                            $number.addClass('rouge');
                        }
                        else {
                            $number.addClass('vert');
                        }
                        $number.html(stock);

                        // Modifier le modal de choix de nombre à l'avance
                        $('#modalQtCup').attr('data-id', result.id);
                        $('#modal_cupcake').modal('show');
                    }
                },
                'JSON'
            );
        }
    });

    /*
        CUPCAKE MODAL (Choix du nombre de cupcake à mettre dans le panier)
    */
    $('#panierCupcake').click(function(elem) {
        $('#modal_ajoutPanier').modal('show');
    });

    /*
        Gérer le changement dans le nombre de cupcake commandé
        Ne pas être < 1 ni au-dessus du max autorisé
     */
    $('#numberCupcake').change(function() {
        verifNumberCupcake(this);
    });

    $('#numberCupcake').keyup(function() {
        verifNumberCupcake(this);
    });
    
    function verifNumberCupcake($source) {
        var max = parseInt($('#modal_ajoutPanier .number').html());
        if($source.value > max) $source.value = max;
        if($source.value < 1) $source.value = 1;
    }

    // L'utilisateur a validé son choix de nombre de cupcakew
    $('#modalQtCup').click(function(elem) {
        $.get(
            'lib/php/ajax/gestionPanier.php',
            {
                type: 'add',
                id: this.dataset['id'].toString(),
                qt: $('#numberCupcake').val()
            },
            function(result) {
                // Ajout réussi
                $('#modal_cupcake').modal('hide');
                if(result == 'true') {
                    var $badge = $('#headerPanier .badge');
                    $badge.html((parseInt($badge.html()) + 1).toString());
                    modalInfo('<h2>Ajout au panier</h2><p>Vous venez d\'ajouter avec succès le cupcake sélectionné à votre panier.</p><p>N\'hésitez pas à y ajouter d\'autres produits !</p>');

                }
                else {
                    modalInfo('<h2>Erreur !</h2><p>Une erreur est survenue lors de l\'ajout au panier du produit sélectionné. Veuillez réessayer plus tard...</p>');
                }
            },
            'text'
        );

    });


    /*
        PANIER
     */
    $('#tablePanier a').click(function(elem){
        elem.preventDefault();
        //console.log(elem);
        //console.log(this);

        $('#modalQuestion').attr('data-id', this.dataset['id']);
        $('#modalQuestion').attr('data-type', 'cupDel');
        $('#modal_question_title').html('Confirmation');
        $('#modal_question_content').html('<p>Êtes-vous sûr de vouloir retirer ce cupcake de votre panier ?');
        $('#modal_question').modal('show');
    });

    $('#modalQuestion').click(function(elem){
        var content;
        var panierId;
        var myModal;

        switch(this.dataset['type']) {
            case 'cupDel':
                // Delete de cupcake d'un panier confirmé
                panierId = this.dataset['id'];
                myModal = this;
                $.get(
                    'lib/php/ajax/gestionPanier.php',
                    {
                        type: 'delete',
                        id: panierId
                    },
                    function(result) {
                        // 'true' si réussi, 'false' si raté pour X raison
                        if(result == 'true') {
                            // Mettre à jour le prix total puis supprimer la ligne du tableau
                            var prixDel = parseFloat($('#panierNonVide td[data-id="'+ panierId +'"]').html());
                            var prixTot = parseFloat($('#priceNumber').html());
                            var $headerPanier = $('#headerPanier span[class="badge"]');
                            var nbPanier = parseInt($headerPanier.html());

                            $('#priceNumber').html((prixTot - prixDel).toString() + '€');
                            $headerPanier.html((--nbPanier).toString());
                            $('#tablePanier tr[data-id="'+panierId+'"]').remove();

                            // Vérifier si la table n'est pas vide
                            if($('#tablePanier tr').length <= 1) {
                                $('#panierVide').css('display', 'block');
                                $('#panierNonVide').css('display', 'none');
                            }

                            content = '<h2>Réussite !</h2><p>L\'élément vient d\'être supprimé avec succès de votre panier.</p>';
                        }
                        else {
                            content = '<h2>Erreur inconnue</h2><p>Une erreur inconnue est survenue lors de la mise à jour de la base de données. Veuillez réessayer.</p>';
                        }

                        modalInfo(content);
                    },
                    'text'
                );
                break;
        }
    });

    /**
     * PANIER
     */
    $('#passerCommande').click(function(elem) {
        var content;
        var peutCommander = true;

        // On a besoin des requêtes synchrones, on réactivera l'async après
        $.ajaxSetup({async: false});

        // Checker si l'user peut commander
        $.post(
            'lib/php/ajax/gestionUser.php',
            {
                type: 'peutCommander'
            },
            function(result) {
                if(result == 'false') {
                    peutCommander = false;
                }
            },
            'text'
        );

        console.log(peutCommander);
        if(!peutCommander) {
            content = '<h2>Attention !</h2><p>Pour pouvoir passer une commande, vous devez impérativement entrer une adresse, un code postal et un pays dans la page de <a href="?p=compte">votre compte</a>.</p>';
            modalInfo(content);
        }
        else {
            var $allCupcakes = $('#tablePanier tr');
            var facture = [];
            var colonnes = ['photo', 'nomProduit', 'quantite', 'prixU', 'prixF', 'osef', 'id'];


            // Récupérer chaque ligne du tableau
            var $tableObject = $allCupcakes.map(function(index) {
                var ligne = {};

                $(this).find('td').each(function(index) {
                    var nomCol = colonnes[index];
                    ligne[nomCol] = $(this).text();
                });

                return ligne;

            }).get();


            // Récupérer les données de chaque ligne
            for(i = 1; i < $tableObject.length; i++) {
                var row = $tableObject[i];
                var id = row['id'].toString();
                var prixF = parseFloat(row['prixF']);
                var prixU = parseFloat(row['prixU']);
                var quantite = parseInt(row['quantite']);

                if(typeof facture[id] === 'undefined') {
                    facture[id] = [];
                    facture[id]['prixF'] = prixF;
                    facture[id]['prixU'] = prixU;
                    facture[id]['qt'] = quantite;
                }
                else {
                    facture[id]['prixF'] += prixF;
                    facture[id]['qt'] += quantite;
                }
            }

            // True si la commande a été modifiée en raison de stock
            var factureModif = false;

            // Synchroniser la commande avec les stocks restants
            $.each(facture, function(index, value) {
                if(typeof value !== "undefined") {
                    // Pour chaque item rassemblé, vérifier que ça ne dépasse pas le stock. Auquel cas, mettre au max du stock restant.
                    $.get(
                        'lib/php/ajax/getCupcakeInfo.php',
                        {
                            id: index
                        },
                        function(result) {
                            if(result.stock == 0) {
                                delete facture[index];
                            }
                            else if(facture[index]['qt'] > result.stock) {
                                facture[index]['qt'] = result.stock;
                                facture[index]['prixF'] = facture[index]['prixU'] * facture[index]['qt'];
                                factureModif = true;
                            }
                        },
                        'JSON'
                    );
                }
                //console.log(index + ' ~ ' + value);
            });

            $.ajaxSetup({async: true});

            // Vérifier si notre panier est vide du coup ou pas
            listeVide = true;
            $.each(facture, function(index, value) {
                if(typeof value !== "undefined") {
                    listeVide = false;
                    return false;       // sortir de la boucle
                }
            });

            //console.log(factureModif);
            console.log(facture);
            content = "<h2>Commande exécutée</h2>";

            if(!listeVide) {

                // Récupérer les valeurs pour les envoyer correctement au script PHP
                var ligneId = "";   // Dans la BDD : "1,3,5"        -> les différents produits achetés
                var ligneQt = "";   // Dans la BDD : "55,1,23"    -> les qt des produits (doit correspondre à la ligne du dessus"
                var prixTotal = 0;  // Prix total après synchronisation

                $.each(facture, function(index, value) {
                    if(typeof value !== "undefined") {
                        if(ligneId.length == 0) {
                            ligneId = index.toString();
                        }
                        else {
                            ligneId += ','+index;
                        }

                        if(ligneQt.length == 0) {
                            ligneQt = facture[index]['qt'].toString();
                        }
                        else {
                            ligneQt += ','+facture[index]['qt'];
                        }
                        prixTotal += facture[index]['prixF'];

                        // Décompter du stock également
                        $.get(
                            'lib/php/ajax/gestionCupcake.php',
                            {
                                type: 'remove',
                                id: index,
                                qt: facture[index]['qt']
                            },
                            function(result) {
                                console.log(result);
                            },
                            'text'
                        );
                    }
                });

                //console.log("> " + ligneId);
                //console.log("> " + ligneQt);
                console.log(prixTotal);

                // Mettre à jour la table commandes

                $.post(
                    'lib/php/ajax/gestionCommandes.php',
                    {
                        type: 'add',
                        idProduits: ligneId,
                        qtProduits: ligneQt,
                        prixTotal: prixTotal

                    },
                    function(result) {
                        console.log(result);
                        if(result == 'true') {
                            if(factureModif) {
                                content += "<p>Cependant, certains objets n'étaient plus en stock lors de la validation de votre commande. Vérifiez les changements sur votre facture.</p><p>Votre panier a été vidé.</p>";
                                modalInfo(content);
                            }
                            else {
                                content += "<p>Votre facture se trouve désormais dans votre espace membre.</p><p>Votre panier a été vidé.</p>";
                                modalInfo(content);
                            }

                            // Vider le panier maintenant que la commande a été exécutée.
                            $.get(
                                'lib/php/ajax/gestionPanier.php',
                                {
                                    type: 'delFromUser'
                                },
                                null,
                                null
                            );

                            $('#panierNonVide').css('display', 'none');
                            $('#panierVide').css('display', 'block');
                            $('#headerPanier .badge').html(0);

                        }
                        else {
                            content = "<h2>Erreur !</h2><p>Une erreur est survenue lors de la création de la commande. Veuillez réessayer plus tard.</p>";
                            modalInfo(content);
                        }
                    },
                    'text'
                );
            }
            else {
                content = "<h2>Attention !</h2><p>Votre commande ne comporte plus aucun objet encore disponible en stock.</p>";
                modalInfo(content);
            }
        }
    });
});