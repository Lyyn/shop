$(document).ready(function(){
    $('.combobox').combobox({

    });


    // Validation du formulaire du compte
    $('#validCompte').click(function(elem) {
        elem.preventDefault();

        // Valeurs du formulaire
        var nom_affichage       = $('#compte_nom_affichage').val();
        var email               = $('#compte_email').val();
        var adresse             = $('#compte_adresse').val();
        var codePostal          = $('#compte_code_postal').val();
        var codePays            = $('#compte_pays').val();
        var formOk = true;

        // Objets jQuery
        var $hintNomAff =   $('#hintCompteNomAffichage');
        var $hintEmail =    $('#hintCompteEmail');
        $hintNomAff.html('');
        $hintEmail.html('');

        // Même règles que pour l'inscription/login
        var regPseudo = /^[\w _\-\+\.]{4,40}$/ig;         // tous les caractères A-Z, espace, car spéciaux, entre 4 et 40 caractères max
        var regMail = /([\w_+\.]{1,})@([\w]){2,}\.([\w]){2,}/ig; // Flemme de décrire

        // On ne vérifie que les valeurs obligatoires : nom d'affichage et email. L'utilisateur peut supprimer son adresse et son pays même si ce serait dommage
        if(nom_affichage.length < 4 || nom_affichage.length > 40) {
            $hintNomAff.html('Votre nom doit contenir entre 4 et 40 caractères.');
            $hintNomAff.css('display', 'block');
            formOk = false;
        }

        if(!regPseudo.test(nom_affichage) && nom_affichage.length > 0) {
            $hintNomAff.html($hintNomAff.html() + '<br/> Votre nom contient des caractères invalides.');
            $hintNomAff.css('display', 'block');
            formOk = false;
        }

        if(!regMail.test(email)) {
            $hintEmail.css('display', 'block');
            $hintEmail.html('Votre e-mail est invalide.');
            formOk = false;
        }

        if(formOk) {
            // Formulaire OK, on met à jour les données de l'user
            $.post(
                'lib/php/ajax/gestionUser.php',
                {
                    type:              'update',
                    nom_affichage:     nom_affichage,
                    email:             email,
                    adresse:           adresse,
                    codePostal:        codePostal,
                    idPays:            codePays
                },
                function(result) {
                    var content;
                    // 'true' si réussi, 'false' si raté
                    if(result == 'true') {
                        content = '<h2>Modifications effectuées</h2><p>Les modifications ont bien été enregistrées dans la base de données.</p>';
                        $('#loginMenu a:first').html(nom_affichage);
                        modalInfo(content);
                    }
                    else {
                        content = '<h2>Erreur !</h2><p>Une erreur est survenue lors de l\'envoi des nouvelles valeurs. Veuillez réessayer un peu plus tard...</p>';
                        modalInfo(content);
                    }
                },
                'text'
            );

        }
        //console.log(nom_affichage + ' - ' + email + ' - ' + adresse + ' - ' + codePostal + ' - ' + codePays);
        //if($.isEmptyObject(codePostal)) console.log('LE CODE POSTAL EST NULL');
    });

    /*
     * Permet d'afficher le modal d'info
     * content : le contenu du modal
     * title : titre (par défaut : information)
     * showModal : si != null, réafficher le modal indiqué
     * reload : si != null, redirige la page vers l'accueil à la fermeture du modal
     */
    function modalInfoShow(content, title, modalToHide) {
        var infoModal = $('#modal_info');
        if(title == null) {
            title = 'Information';
        }

        $('#modal_info_content').html(content);
        $('#modal_info_title').html(title);

        if(modalToHide != null) {
            modalToHide.modal('hide');
            infoModal.on('hidden.bs.modal', function() {
                modalToHide.modal('show');
                infoModal.off('hidden.bs.modal', null);
            });
        }

        infoModal.modal('show');
    }
    function modalInfo(content) {
        modalInfoShow(content, null, null);
    }

});