<?php
    session_start();
    if(!empty($_SESSION['id_users'])) {
        header('Content-type:application/pdf');
    }
    else {
        die();
    }


    // Modules requis
    require('admin/dbConnect.php');
    require('lib/php/classes/CommandesDB.class.php');
    require('lib/php/classes/CupcakeDB.class.php');
    require('lib/php/classes/UsersDB.class.php');
    require('lib/php/modules/fpdf/fpdf.php');


    if(!empty($connexion)) {
        $factureFetch = false;

        if(!empty($_GET['id'])) {
            $facture = new CommandesDB($connexion);
            $factureFetch = $facture->read($_GET['id']);
        }

        if($factureFetch) {
            $userDB = new UsersDB($connexion);
            $userDB->read($_SESSION['id_users']);
            $user = $userDB->getUser();
            $prixT = 0;

            $commande = $facture->getCommande();

            $pdf = new FPDF();
            $pdf->AddPage();
            //$pdf->SetAutoPageBreak(true);

            // Infos client + entreprise
            $l = 167;
            $h = 8;
            $pdf->SetFont('Arial', '', 9);
            $pdf->Cell($l, $h, utf8_decode($user->getNomAffichage()));
            $pdf->Cell(0, $h, 'TopKek Corp.', 0, 0, 'R');
            $pdf->Ln(5);
            $pdf->Cell($l, $h, utf8_decode($user->getAdresse()));
            $pdf->Cell(0, $h, 'Sentier de la farine, 46', 0, 0, 'R');
            $pdf->Ln(5);
            $pdf->Cell($l, $h, utf8_decode('Code postal : '.$user->getCodePostal()));
            $pdf->Cell(0, $h, 'Code postal : 1337', 0, 0, 'R');
            $pdf->Ln(5);
            $pdf->Cell($l, $h, utf8_decode($user->getEmail()));
            $pdf->Cell(0, $h, utf8_decode('cup@ca.ke'), 0, 0, 'R');


            // Numéro de facture et date
            $pdf->SetFont('', '', 20);
            $pdf->Image('lib/images/logopdf.png', 80, 0, 50);
            $pdf->SetY(($pdf->GetY() + 45));
            $pdf->MultiCell(0, $h, utf8_decode('Facture N°'.$commande->getIdCommandes()), 0, 'C');
            $pdf->Ln(0);
            $pdf->SetFont('', '', 10);
            $pdf->Cell(0, $h, $commande->getDateCommande(), 0, 0, 'C');
            $pdf->Ln(10);


            // Headers
            $pdf->SetFillColor(98, 55, 41); // Brun
            $pdf->SetDrawColor(98, 55, 41);
            $pdf->SetTextColor(255, 255, 255);  // Blanc
            $pdf->SetLineWidth(0.3);
            $pdf->SetFont('', 'B', 10); // Gras
            $colonnes = array('Nom du produit', utf8_decode('Quantité'), 'Prix unitaire', 'Prix total');
            $padding = array(47, 47, 47, 47);   // padding des cellules

            // Génération des headers
            for($i=0; $i < count($padding); $i++) {
                $pdf->Cell($padding[$i], $h, $colonnes[$i], 1, 0, 'C', true);
            }

            // Passer à la ligne pour quitter celle des headers
            $pdf->Ln();

            // Génération des cellules
            $pdf->SetFillColor(199, 243, 206);  // Vert léger
            $pdf->SetTextColor(98, 55, 41);     // Brun
            $pdf->SetFont('');
            $fond = false;
            $idProduits = $commande->idProduitsToArray();
            $qtProduits = $commande->qtProduitsToArray();
            $euro = chr(128);

            //print_r($idProduits);
            //print_r($qtProduits);
            // On parse chaque id de produit de la commande
            for($i=0; $i < count($idProduits); $i++) {
                $cupcakeDB = new CupcakeDB($connexion);
                $cupcakeDB->read($idProduits[$i]);
                $cupcake = $cupcakeDB->getCupcake();

                $pdf->Cell($padding[0], $h, '   '.$cupcake->getNomProduit(), 'LR', 0, 'L', $fond);
                $pdf->Cell($padding[0], $h, '   '.$qtProduits[$i].utf8_decode(' unité(s)'), 'LR', 0, 'L', $fond);
                $pdf->Cell($padding[0], $h, '   '.$cupcake->getPrix().' '.$euro, 'LR', 0, 'L', $fond);
                $pdf->Cell($padding[0], $h, '   '.($qtProduits[$i] * $cupcake->getPrix()).' '.$euro, 'LR', 0, 'L', $fond);
                $prixT += ($qtProduits[$i] * $cupcake->getPrix());
                $pdf->Ln();
                $fond = !$fond;
            }
            $pdf->Cell(array_sum($padding), 0, '', 'T');

            // Affichage du prix final
            $pdf->Ln(10);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('', 'B', 20);
            $pdf->Cell(170, 8, 'Prix final :', 0, 0, 'R');
            $pdf->SetFont('', '', 14);
            $pdf->Cell(0, 9, $prixT.' '.$euro, 0, 0, 'L');
            $pdf->Output();
        }
    }
?>