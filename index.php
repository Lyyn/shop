<?php
    // Garder ou ouvrir une session
    session_set_cookie_params(604800, '/cupcake/');
    session_start();

    // Quelques variables utiles
    $PAGEROOT = 'lib/php/pages/';
    $DEBUG = false;                  // /!\ Retirer en prod

    // Chargement des fonctions et classes
    include_once('admin/dbConnect.php');
    include_once('lib/php/classes/PanierDB.class.php');
    include_once('lib/php/classes/CupcakeDB.class.php');
    include_once('lib/php/utils/funcs.php');

    // Panier
    // Pas de panier en invité. Le panier sera toujours récupéré et stocké côté DB
    if(!empty($_SESSION['id_users'])) {
        $panier = new PanierDB($connexion);
        $panier->read($_SESSION['id_users']);
    }
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>TopKek - Le TOP des CAKES !</title>
        <!-- Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'> <!-- font-family: 'Open sans', sans-serif; -->
        <link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>  <!-- font-family: 'Lobster', cursive; -->
        
        <!-- CSS -->
        <link rel="stylesheet" href="lib/css/bootstrap-theme.css" type="text/css"/>
        <link rel="stylesheet" href="lib/css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="lib/css/bootstrap-combobox.css" type="text/css"/>

        <!-- Cupcake -->
        <link rel="stylesheet" href="lib/css/cupcake_base.css" type="text/css"/>
        <link rel="stylesheet" href="lib/css/modal_base.css" type="text/css"/>
        <link rel="stylesheet" href="lib/css/cupcake_media.css" type="text/css"/>
    </head>
    <body>
        <header>
        </header>
        <div id="menu">
                <ul>
                    <li><a href="?">Accueil</a></li>
                    <li><a href="?p=shop">Magasin</a></li>
                    <?php
                        if(!empty($_SESSION['nom_affichage'])) {
                            echo '<li id="loginMenu"><a href="?p=compte">'.$_SESSION['nom_affichage'].'</a> - <a href="" id="loginLink" data-toggle="modal" data-target="#modal_logoff">Se déconnecter</a></li>';
                        }
                        else {
                            echo '<li id="loginMenu">Anonyme - <a href="" id="loginLink" data-toggle="modal" data-target="#modal_login">Se connecter</a></li>';
                        }

                        $panierCount = -1;
                        if(!empty($_SESSION['id_users'])) {
                            $panierCount = $panier->getPanier()->getPanierSize();
                        }
                        echo '<a href="?p=panier"><span id="headerPanier" style="display: '.(($panierCount == -1) ? 'none' : 'inline' ).';" ><span class="glyphicon glyphicon-shopping-cart"></span> <span class="badge">  '.(($panierCount == null) ? 0 : $panierCount ).'</span></span></a>';

                        if(!empty($_SESSION['state'])) {
                            if($_SESSION['state'] == 42) {
                                echo '<li class="admin" id="menuAdmin"><a href="?p=admCake">Admin</a></li>';
                            }
                        }

                    ?>
                </ul>
            </div>
        <div class="container">
            <!-- contenu -->
            <?php
                if($connexion == null) {
                    include('lib/php/pages/crashDB.php');
                }
                else {
                    if(!isset($_GET['p'])) {
                        include($PAGEROOT.'index.php');
                    }
                    else {
                        if(file_exists($PAGEROOT.$_GET['p'].'.php') && ($_GET['p'] != 'crashDB' || $DEBUG)) {
                            include($PAGEROOT.$_GET['p'].'.php');
                        }
                        else {
                            include($PAGEROOT.'404.php');
                        }
                    }
                }
            ?>
        </div>
        <footer>
            <div class="row">
                <div class="col-md-4 first">
                    <img src="lib/images/cupcake-footer.png" alt="cupcake footer"/>
                </div>
                <div class="col-md-4 second">
                    <p>Ce site est un projet scolaire et ne propose pas véritablement des produits.</p>
                    <p>Les sources des ressources utilisées peuvent être trouvée ici : <a href="?p=sources">Sources</a></p>
                </div>
                <div class="col-md-4 third">
                    <p>Hendrickx Matthias</p>
                    <p>3I - HEPH Condorcet 2015-2016</p>
                    <p>Technologies utilisées :</p>
                    <ul>
                        <li>HTML5</li>
                        <li>CSS3</li>
                        <li>Bootstrap</li>
                        <li>jQuery</li>
                        <li>AJAX</li>
                    </ul>
                </div>
            </div>
        </footer>
        <?php
            // Modals
            include('lib/php/pages/modals.php');

            // DEBUG ~
            if($DEBUG) {
                ?>
                <debug>
                    <pre>
                        <?php
                            var_dump($GLOBALS);
                        ?>
                </pre>
                </debug>
                <?php
               /*
               if(!isset($_GET['p'])) {
                    echo('Pas de page indiquée, affichage de l\'accueil');
                }
                else {
                    if(file_exists($PAGEROOT.$_GET['p'].'.php') && ($_GET['p'] != 'crashDB' || $DEBUG)) {
                        echo('Page demandée et trouvée : '.$_GET['p']);
                    }
                    else {
                        echo('Page demandée mais non trouvée, affichage de l\'accueil');
                        if(file_exists($PAGEROOT.$_GET['p'].'.php')) echo('<br/>Fichier existant');
                    }
                }
               */
            }
        ?>

        <script src="lib/js/jquery-1.11.3.min.js"></script>
        <script src="lib/js/bootstrap.min.js"></script>
        <script src="lib/js/bootstrap-combobox.js"></script>
        <script src="lib/js/cupcake.js"></script>
        <script src="lib/js/cupcake_compte.js"></script>
    </body>
</html>
