<?php
/**
 * Created by PhpStorm.
 * User: Lyyn
 * Date: 25-09-15
 * Time: 12:04
 */
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Bootstrap Shop</title>
        <link rel="stylesheet" href="lib/css/bootstrap-theme.css" type="text/css"/>
        <link rel="stylesheet" href="lib/css/bootstrap.css" type="text/css"/>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="well">
                        .col-md-4
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-4">
                    <div class="well">
                        .col-md-4 offset
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="well">
                        <div class="col-md-4">
                            première col
                        </div>
                    </div>
                    <div class="well">
                        <div class="col-md-offset-4">
                            dernière col
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="row show-grid">
                <div class="col-md-1">
                    Bootstrap
                </div>
                <div class="col-md-2">
                    Bootstrap
                </div>
                <div class="col-md-3">
                    Bootstrap
                </div>
                <div class="col-md-3">
                    Bootstrap
                </div>
            </div>

            <div class="row show-grid">
              <div class="col-md-1">.col-md-1</div>
              <div class="col-md-1">.col-md-1</div>
              <div class="col-md-1">.col-md-1</div>
              <div class="col-md-1">.col-md-1</div>
              <div class="col-md-1">.col-md-1</div>
              <div class="col-md-1">.col-md-1</div>
              <div class="col-md-1">.col-md-1</div>
              <div class="col-md-1">.col-md-1</div>
              <div class="col-md-1">.col-md-1</div>
              <div class="col-md-1">.col-md-1</div>
              <div class="col-md-1">.col-md-1</div>
              <div class="col-md-1">.col-md-1</div>
            </div>

    <!-- Scripts JS -->
    <script src="lib/js/jquery-1.11.3.min.js"></script>
    <script src="lib/js/bootstrap.min.js"></script>
    </body>
</html>
